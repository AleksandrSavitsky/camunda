package com.example.democamundahr.task;

import com.example.democamundahr.service.UnitService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Component("GetManager")
public class GetManager implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        String managerId = UnitService.getUnitHead((int) execution.getVariable("unit"));
        execution.setVariable("manager", managerId);
    }
}
