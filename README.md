## Описание

На основе REST-сервисов, созданных на занятиях 1 и 2 создать процесс приема нового сотрудника

Для создания процесса использовать Camunda в варианте spring-boot-starter
В качестве инструмента сборки использовать Gradle

Процесс:
1. Процесс стартует при получении REST-запроса, с указанием:
- ФИО сотрудника
- позиции (id)
- подразделения (id)
- зарплаты
- даты приема
2. Запрос должен быть назначен на рассмотрение сотрудника HR, который проверяет корректность данных и вносит данные по зарплате сотрудника
3. В случае, если сотрудник принимается на позицию руководителя (id позиции 1), выполнить регистрацию сотрудника и обновить данные по подразделению (проставить id сотрудника как id руководителя)
4. Иначе - направить запрос на рассмотрение руководителю подразделения, в которое принимается сотрудник
5. После подтверждения руководителем - выполнить регистрацию сотрудника
6. В обоих случаях после успешной регистрации - создать пользователя в Camunda, присвоить ему начальный пароль "password", и назначить на него задачу с его данными для просмотра.
7. После подтверждения сотрудником - процесс завершается.

**Вопросы**

*1. Рассказать об элементах build.gradle. Что значат использованные нотации зависимостей?*

*2. Что нужно сделать, чтобы данные в БД сохранялись между запусками? Почему?*

*3. Как подключить другую БД?*

*4. Какие задачи выполняет BPMN-движок в нашем приложении?*

*5. Что такое границы транзакций в Camunda? Как можно ими управлять? Какие узлы являются асинхронными по умолчанию?*


---

## Задание 
Создать собственный проект - процесс регистрации отпуска сотрудником.
Шаги процесса:
1. Сотрудник запрашивает отпуск, входящие данные - id сотрудника, дата начала отпуска, дата окончания отпуска
2. Необходимо проверить:
- что у сотрудника есть достаточное количество дней отпуска. Рассчитывается как 28 дней на каждый год от даты приема. Например, если дата приема - 1 января, то до 31 декабря ему доступно 28 дней. Если у сотрудника в этом периоде уже было 20 дней отпуска, то он может заказать не более 8 дней.
- что запрошенный период не пересекается с уже одобренным отпуском сотрудника того же подразделения.
3. Если проверка не пройдена - вернуть ответ "Недостаточно дней" или "Отпуск запрошен на те же даты уже зарегистрированного отпуска сотрудника <ФИО>" и завершить процесс
4. Если проверка пройдена, запрос поступает на согласование руководителю отдела, или если это руководтель - сотруднику HR
5. Если руководитель или HR отклонил заявку - Проставить признак "отклонена" и перевести заявку на сотрудника, после ознакомления процесс завершается.
6. Если заявка одобрена - создать запись в таблице отпусков (см. задания 1-2), перевести заявку с признаком "одобрена" на сотрудника, после ознакомления процесс завершается.